package uz.pdp.warehouseprojectlesson11.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.warehouseprojectlesson11.entity.attachment.Attachment;

public interface AttachmentRepo extends JpaRepository<Attachment, Long> {
}
