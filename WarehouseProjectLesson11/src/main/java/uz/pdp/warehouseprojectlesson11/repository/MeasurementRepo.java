package uz.pdp.warehouseprojectlesson11.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.warehouseprojectlesson11.entity.Measurement;

public interface MeasurementRepo extends JpaRepository<Measurement, Long> {


    boolean existsByName(String name);

}
