package uz.pdp.warehouseprojectlesson11.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.warehouseprojectlesson11.entity.attachment.AttachmentContent;

public interface AttachmentContentRepo extends JpaRepository<AttachmentContent, Long> {
}
