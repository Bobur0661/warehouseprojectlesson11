package uz.pdp.warehouseprojectlesson11.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.warehouseprojectlesson11.entity.InputProduct;
import uz.pdp.warehouseprojectlesson11.entity.OutputProduct;

public interface OutputProductRepo extends JpaRepository<OutputProduct, Long> {
}
