package uz.pdp.warehouseprojectlesson11.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.warehouseprojectlesson11.entity.Supplier;

public interface SupplierRepo extends JpaRepository<Supplier, Long> {

    boolean existsByPhoneNumber(String phoneNumber);

}
