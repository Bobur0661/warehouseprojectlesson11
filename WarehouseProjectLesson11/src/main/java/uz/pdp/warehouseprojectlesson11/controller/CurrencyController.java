package uz.pdp.warehouseprojectlesson11.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.warehouseprojectlesson11.entity.Currency;
import uz.pdp.warehouseprojectlesson11.entity.Measurement;
import uz.pdp.warehouseprojectlesson11.payload.ApiResponse;
import uz.pdp.warehouseprojectlesson11.repository.CurrencyRepo;
import uz.pdp.warehouseprojectlesson11.repository.MeasurementRepo;
import uz.pdp.warehouseprojectlesson11.service.CurrencyService;
import uz.pdp.warehouseprojectlesson11.service.MeasurementService;

import java.util.List;

@RestController
@RequestMapping("/currency")
public class CurrencyController {

    @Autowired
    CurrencyService currencyService;

    @Autowired
    CurrencyRepo currencyRepo;


    @GetMapping("/all")
    public List<Currency> get() {
        return currencyRepo.findAll();
    }


    @PostMapping("/addOrEdit")
    public ApiResponse addOrEdit(@RequestBody Currency currency) {
        return currencyService.addOrEdit(currency);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return currencyService.delete(id);
    }

}
