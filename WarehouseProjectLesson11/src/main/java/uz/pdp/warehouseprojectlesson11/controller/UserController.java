package uz.pdp.warehouseprojectlesson11.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import uz.pdp.warehouseprojectlesson11.entity.User;
import uz.pdp.warehouseprojectlesson11.payload.ApiResponse;
import uz.pdp.warehouseprojectlesson11.payload.UserDto;
import uz.pdp.warehouseprojectlesson11.repository.UserRepo;
import uz.pdp.warehouseprojectlesson11.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {


    @Autowired
    UserRepo userRepo;

    @Autowired
    UserService userService;

    // Get All Users
    @GetMapping("/all")
    public List<User> getAll() {
        return userRepo.findAll();
    }


    // Get by User id
    @GetMapping("/{id}")
    public ApiResponse getById(@PathVariable Long id) {
        return userService.getById(id);
    }


    @PostMapping("/save")
    public ApiResponse save(@RequestBody UserDto dto) {
        return userService.save(dto);
    }


    @PutMapping("/update/{id}")
    public ApiResponse update(@PathVariable Long id, @RequestBody UserDto dto) {
        return userService.update(id, dto);
    }


    @DeleteMapping("/delete/{id}")
    public ApiResponse delete(@PathVariable Long id) {
        return userService.delete(id);
    }
}
