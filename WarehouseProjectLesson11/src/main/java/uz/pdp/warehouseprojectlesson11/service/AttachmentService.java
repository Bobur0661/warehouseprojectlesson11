package uz.pdp.warehouseprojectlesson11.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.pdp.warehouseprojectlesson11.entity.attachment.Attachment;
import uz.pdp.warehouseprojectlesson11.entity.attachment.AttachmentContent;
import uz.pdp.warehouseprojectlesson11.payload.ApiResponse;
import uz.pdp.warehouseprojectlesson11.repository.AttachmentContentRepo;
import uz.pdp.warehouseprojectlesson11.repository.AttachmentRepo;

import java.io.IOException;
import java.util.Iterator;

@Service
public class AttachmentService {

    @Autowired
    AttachmentRepo attachmentRepo;

    @Autowired
    AttachmentContentRepo attachmentContentRepo;


    public ApiResponse uploadFile(MultipartHttpServletRequest request) throws IOException {
        Iterator<String> fileNames = request.getFileNames();
        MultipartFile file = request.getFile(fileNames.next());

        // Attachment ga nomini, size ni, ContentType ni sqlaymiz.
        Attachment attachment = new Attachment();
        attachment.setName(file.getOriginalFilename());
        attachment.setSize(file.getSize());
        attachment.setContentType(file.getContentType());
        Attachment savedAttachment = attachmentRepo.save(attachment);

        // Bytes ==> asosoiy mag'izini saqlaymiz
        AttachmentContent attachmentContent = new AttachmentContent(file.getBytes(), savedAttachment);
        attachmentContentRepo.save(attachmentContent);
        return new ApiResponse(true, "Saved", savedAttachment.getId());
    }

}
